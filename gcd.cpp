#include <iostream>
#include <math.h>
using namespace std;
int GCD(int a, int b);

int main () {
	cout << "Hello World! GCD\n";
	int x, y;

	cout << "Value 1: ";
	cin >> x;
	cout << "Value 2: ";
	cin >> y;

	cout << "\nThe Greatest Common Divisor of "
	     << x << " and " << y << " is " << GCD(x, y) << endl;  

	return 0;
}

int GCD(int x, int y){
	if ( y == 0 ) 
		return x;
	else if ( x >= y && y > 0 ) 
		return GCD(y, x%y);
	else 
		return -1;
	/*
    while( 1 ) {
		if( a % b == 0 )
			return b;
		if( b % a == 0 )
			return a;
	}
	*/
}

 