#include <iostream>
 
using namespace std;
 
//Prototype funcitons
char printarray(char board[3][3]); //Function to print the board
int winnercheck(char board[3][3], char letter, int *won); //Checks if someone has won the game
int move(char board[3][3], char letter, int *won); //Collects coordinates, validates input and uses above functions
 
 
int main()
{
    char board[3][3]={'-','-','-','-','-','-','-','-','-'}; //Initial board
    char board_start[4][4]={'-','1','2','3','1','-','-','-','2','-','-','-','3','-','-','-'}; //Showing input method (coordinates)
    char X,O; //Player 'pieces'
    int i,j; //For printing board_start array
    int x,y; //For placing X's and O's
    int won=0; //This will be set to 0 if no one has won and 1 is there is a winner. Changed by the winnercheck function.
    int turn=1; //Turn is set to 1 as Player 1 goes 1st
 
    //Introduction
    cout<<"Welcome to Tic-Tac-Toe\n";
    for(i=0;i<4;i++) //Show game board coordinates
    {
        for(j=0;j<4;j++)
        {
            cout<<board_start[i][j]<<"\t";
        }
        cout<<"\n";
    }
    cout<<"Instructions: when asked, input the coordinates of where you want to go\n";
    cout<<"in the form of x y, where x is the row number(1-3) and y is the column\n";
    cout<<"number(1-3)\n";
    cout<<"e.g. to go in the middle square would be 2 2, or top right corner would be 1 3\n";
    cout<<"Player 1=X, Player 2=O" <<endl<<endl;
    //End of introduction
 
    //Game
    while(won==0)
    {
        switch (turn)
        {
            case 1:
                cout<<"Player 1, enter where you want to go: ";
                move(board,'X', &won);
                if(won==1) cout<<"Player 1 wins\n";
                turn=2;
                break;
            case 2:
                cout<<"Player 2, enter where you want to go: ";
                move(board,'O', &won);
                if(won==1) cout<<"Player 2 wins\n";
                turn=1;
                break;
        }
    }
}
 
char printarray(char board[3][3])
{
    int i,j;
 
    cout<<"\n";
 
    for(i=0;i<3;i++)
    {
        for(j=0;j<3;j++)
        {
            cout<<board[i][j]<<"\t";
        }
        cout<<"\n";
    }
}
 
int move(char board[3][3], char letter, int *won)
{
    int x,y,range,check;
 
    cin>> x>>y;
    x-=1; y-=1;
    range = x<0 || x>2 || y<0 || y>2;
    check = board[x][y] == 'X' || board[x][y] == 'O';
 
    while(range || check)
    {
        if (range) cout<<"Coordinates out of range. Try again: ";
        if (check) cout<<"Position already taken. Try again: ";
        cin>> x >>y;
        x-=1; y-=1;
        range = x<0 || x>2 || y<0 || y>2;
        check = board[x][y] == 'X' || board[x][y] == 'O';
    }
    board[x][y] = letter;
    printarray(board);
    winnercheck(board, letter, won);
}
 
int winnercheck(char board[3][3], char letter, int *won)
{
    if(board[0][0] == letter && board[1][0] == letter && board[2][0] == letter) *won=1;
    if(board[0][1] == letter && board[1][1] == letter && board[2][1] == letter) *won=1;
    if(board[0][2] == letter && board[1][2] == letter && board[2][2] == letter) *won=1;
    if(board[0][0] == letter && board[0][1] == letter && board[0][2] == letter) *won=1;
    if(board[1][0] == letter && board[1][1] == letter && board[1][2] == letter) *won=1;
    if(board[2][0] == letter && board[2][1] == letter && board[2][2] == letter) *won=1;
    if(board[0][0] == letter && board[1][1] == letter && board[2][2] == letter) *won=1;
    if(board[0][2] == letter && board[1][1] == letter && board[2][0] == letter) *won=1;
}