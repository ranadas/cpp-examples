#include <iostream>
#include <math.h>
using namespace std;
bool isPrime(int n);
bool isPrimeFast(int n);

int main () {
  cout << "Hello World!\n";
  int number;
  cin >> number;
  cout << "All prime numbers less than: "<<number << " are:\n";
  for (int i=0;i <= number;i++){
	cout<< i <<" is Prime : " <<(isPrimeFast(i)==1?" Yes":" No")<<"\n";
  }
  return 0;
}

 bool isPrime(int i) {
	if (i < 2) {
       return false;
    } else if (i % 2 == 0 && i != 2) {
       return false;
    } else {
       for (int j = 3; j <= sqrt(i); j = j + 2) {
          if (i % j == 0) {
              return false;
          }
       }
       return true;
    }
}

bool isPrimeFast(int num) {
	if(num<=1)
		return false;
	for(int i=2; i<num; i++)	{
		if(num%i==0)
			return false;
	}
	return true;
}
